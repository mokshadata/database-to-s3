from setuptools import setup

install_requires = [
    'boto3>=1.18.44',
    'botocore>=1.21.44',
    'jmespath>=0.10.0',
    'python-dateutil>=2.8.2',
    's3transfer>=0.5.0',
    'six>=1.16.0',
    'smart-open>=5.2.1',
    'urllib3>=1.26.6',
]

setup(
    name='database_to_s3',
    version='0.9',
    packages=['database_to_s3'],
    long_description='Reads tables from a database into csv files in S3',
    install_requires=install_requires,
)
