import io
import os
import csv
import logging
import sys
import pyodbc
import smart_open
import boto3

logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(message)s')
logger = logging.getLogger(__name__)


def get_table_spec(conn, table_name):
    sql = """
        SELECT
            column_name,
            data_type
        FROM information_schema.columns
        WHERE TABLE_NAME='{table_name}'
        ORDER BY ordinal_position ASC""".format(table_name=table_name)
    with conn.cursor() as cursor:
        cursor.execute(sql)
        columns = cursor.fetchall()
        return columns


def get_select_query(db_type, table_name):
    query_index = {
        'sqlserver': """
            SELECT
                *
            FROM
                "{table_name}"
        """.format(table_name=table_name),
        'mysql': """
            SELECT
                *
            FROM
                `{table_name}`
        """.format(table_name=table_name)
    }

    return query_index[db_type]


def stream_table_to_s3(conn, sql, table_name, s3_path, table_spec, session, batch_size=2000):
    with conn.cursor() as cursor:
        cursor.execute(sql)
        output = io.StringIO()
        writer = csv.writer(output)
        writer.writerow([c[0] for c in table_spec])  # header
        s3_path = '{s3_path}{table_name}.csv'.format(s3_path=s3_path,
                                                     table_name=table_name.lower().replace(' ', '_'))
        with smart_open.open(s3_path, 'wb', transport_params={'client': session.client('s3')}) as s3_out:
            batch_count = 1
            bytes_written = 1
            rows_written = 0
            while bytes_written:
                batch = cursor.fetchmany(batch_size)
                writer.writerows(batch)
                s_output = output.getvalue()
                logger.debug('writing batch {c}'.format(c=str(batch_count)))
                bytes_written = s3_out.write(s_output.encode())

                output.truncate(0)
                output.seek(0)
                batch_count += 1
                rows_written += batch_size
                logger.debug(f'Rows written: {rows_written}')
        logger.info(f'Table written to: {s3_path}')
        return rows_written


def get_db_driver(db_type):
    driver_index = {
        'sqlserver': '{ODBC Driver 17 for SQL Server}',
        'mysql': '{MySQL ODBC 8.0 Driver}',
        'postgres': '{PostgreSQL Unicode}'
    }
    return driver_index.get(db_type, '')


def table_to_s3():
    server = os.getenv('DB_HOST')
    database = os.getenv('DB_NAME')
    username = os.getenv('DB_USERNAME', '')
    if not username:
        username = os.getenv('DB_CREDENTIAL_USERNAME', '')
    password = os.getenv('DB_PASSWORD', '')
    if not password:
        password = os.getenv('DB_CREDENTIAL_PASSWORD', '')
    db_type = os.getenv('DB_TYPE')
    driver = get_db_driver(db_type)
    if not driver:
        raise Exception(f'Error: invalid db_type: {db_type} Choose one of these: [sqlserver, mysql, postgres]')
    conn = pyodbc.connect('DRIVER='+driver+';SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+password)
    session = boto3.session.Session(
        aws_access_key_id=os.getenv('AWS_CREDENTIAL_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_CREDENTIAL_SECRET_ACCESS_KEY')
    )
    table_names = os.getenv('TABLE_NAMES').split('\n')
    s3_path = os.getenv('S3_PATH')
    error_count = 0
    for table_name in table_names:
        try:
            if not table_name:
                continue
            logger.info(f'Streaming table to S3: {table_name}')
            table_spec = get_table_spec(conn, table_name)
            sql = get_select_query(db_type, table_name)
            rows_written = stream_table_to_s3(conn, sql, table_name, s3_path, table_spec, session)
            logger.info(f'Successfully wrote ~{rows_written} rows')
        except Exception as e:
            logger.error(f'Error: failed to write table {table_name}')
            logger.error(e)
            error_count += 1

    conn.close()
    if error_count > 0:
        raise Exception(f'Error: {error_count} tables failed to load')


if __name__ == "__main__":
    table_to_s3()
